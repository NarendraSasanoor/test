<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblUserContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tblUserContacts', function (Blueprint $table) {
            $table->bigIncrements('contact_id');
            $table->string('contact_name');
            $table->string('contact_number')->unique();
            $table->string('contact_email')->unique();
            $table->integer('user_id');
            $table->integer('is_deleted');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tblUserContacts');
    }
}
