$(document).ready(function(){
    $(".close_popup,#createcontact").click(function(){
        $("#contact_form")[0].reset();

    });
});
$(document).on("click","#contact_save",function(e){
    e.preventDefault();
    var obj = {
        id : $("#contact_id").val(),
        name : $("#contact_name").val(),
        email : $("#contact_email").val(),
        number : $("#contact_number").val(),
    }
    // $("#close_popup").trigger("click");

    $.ajax({
        url: '/save_contact',
        type: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {

            encrypted : (obj)
        },        
        success: function(data) {
            if (data.error) {
                alert(data.message);
            }else if(data.success){
                $("#close_popup").trigger("click");
                window.location.reload();
            }
        },
        
    });
});
$(document).on("click",".edit_contact",function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");
    $.ajax({
        url: '/view_contact',
        type: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            encrypted : btoa(id)
        },        
        success: function(data) {
            if (data.error) {
                alert(data.message);
            }else if(data.success){
                $("#createcontact").trigger("click");
                $("#contact_id").val(data.contactdata[0]['contact_id']);
                $("#contact_name").val(data.contactdata[0]['contact_name']);
                $("#contact_email").val(data.contactdata[0]['contact_email']);
                $("#contact_number").val(data.contactdata[0]['contact_number']);
                // window.location.reload();
            }
            
        },
        
    });
});
$(document).on("click",".view_contact",function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");
    $.ajax({
        url: '/view_contact',
        type: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            encrypted : btoa(id)
        },        
        success: function(data) {
            if (data.error) {
                alert(data.message);
            }else if(data.success){
                $("#exampleModalView").modal("show");
                $(".contact_name_view").html(data.contactdata[0]['contact_name']);
                $(".contact_name_email").html(data.contactdata[0]['contact_email']);
                $(".contact_name_number").html(data.contactdata[0]['contact_number']);
                // window.location.reload();
            }
            
        },
        
    });
});
$(document).on("click",".delete_contact",function(e){
    e.preventDefault();
    var id = $(this).attr("data-id");
    $.ajax({
        url: '/delete_contact',
        type: "post",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
            encrypted : btoa(id)
        },        
        success: function(data) {
            if (data.error) {
                alert(data.message);
            }else if(data.success){
                window.location.reload();
            }
            
        },
        
    });
});