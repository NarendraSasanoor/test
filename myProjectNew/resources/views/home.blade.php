@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    @isset($id)
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" id="createcontact">
            Create contact
        </button>
        <table class="table table-bordered mt-3">
            <thead>
                <th class="">Sr.No</th>
                <th class="">Name</th>
                <th class="">Emai Id</th>
                <th class="">Contact Number</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach($contadetails as $key => $value)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$value['contact_name']}}</td>
                    <td>{{$value['contact_email']}}</td>
                    <td>{{$value['contact_number']}}</td>
                    <td><button type="button" data-id = "<?php print_r($value['contact_id'])?>" class="view_contact btn btn-info">View</button><button  type="button" data-id = "<?php print_r($value['contact_id'])?>" class="edit_contact btn btn-secondary">Edit</button><button  type="button" data-id = "<?php print_r($value['contact_id'])?>" class="delete_contact btn btn-danger">Delete</button></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    @endisset
</div>
<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contact Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
            <form id="contact_form">
                <div class="row">
                    <div class="col-md-4">
                        Name
                    </div>
                    <div class="col-md-8">
                        <input type="hidden" name="contact_id" id="contact_id">
                        <input type="text" name="contact_name" id="contact_name" required maxlength="255">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        Email Id
                    </div>
                    <div class="col-md-8">
                        <input type="email" name="contact_email" id="contact_email" required maxlength="255">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        Contact Number
                    </div>
                    <div class="col-md-8">
                        <input type="text" name="contact_number" id="contact_number" required maxlength="10">
                    </div>
                </div>
            </form>
                
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close_popup" class="close_popup">Close</button>
        <button type="button" class="btn btn-primary" id="contact_save">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModalView" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelView" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabelView">Contact Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-4">
                Name
            </div>
            <div class="col-md-8">
                <span class="contact_name_view"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                Email Id
            </div>
            <div class="col-md-8">
                <span class="contact_name_email"></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                Contact Number
            </div>
            <div class="col-md-8">
                <span class="contact_name_number"></span>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="close_popup">Close</button>
      </div>
    </div>
  </div>
</div>
@endsection
<!-- <script type="text/javascript" src="{{URL::asset('js/user.js?')}}"></script> -->
@section('script')
<script type="text/javascript" src="{{URL::asset('js/user.js?')}}"></script>
@endsection