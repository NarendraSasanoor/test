<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Models\UserContact;
use Carbon;
use DB;
use Validator;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id = Auth::id();
        $contadetails = UserContact::where('user_id',$id)->where("is_deleted",0)->get()->toArray();
        return view('home',['id'=>$id,'contadetails'=>$contadetails]);
    }
    public function contactSave(Request $request){
        $encrypted = $request->encrypted;
        $decryptedData = ($encrypted);
        $id = Auth::id();
        
        $created_on = Carbon::now();
        $validator = Validator::make($decryptedData, [
            'name'        => array('required', "regex:/^[a-zA-Z0-9-'&._\s]{1,100}$/"),
            'number'    => array('required', 'regex:/[+1-9]{1}[-0-9]{6,11}/'),       
            'email'    => array('required'),      
        ]);
        $attributeNames = array(
            'name'        => 'Contact Name',
            'number'    => 'Contact Number',
            'email'    => 'Contact email'
        );
        $validator->setAttributeNames($attributeNames);

        if($validator->fails()){
            return response()->json(['error' => 1, 'message' => "please fill the details properly"]);
        }
        DB::beginTransaction();

        try{
            if ($decryptedData['id'] != "") {
                $dataarray = [
                    'contact_name' => $decryptedData['name'],
                    'contact_number' => $decryptedData['number'],
                    'contact_email' => $decryptedData['email'],
                    'user_id' => $id,
                    'updated_at' => $created_on
                ];
                UserContact::where('contact_id',$decryptedData['id'])->update($dataarray);
            }else{
                $dataarray = [
                    'contact_name' => $decryptedData['name'],
                    'contact_number' => $decryptedData['number'],
                    'contact_email' => $decryptedData['email'],
                    'user_id' => $id,
                    'created_at' => $created_on,
                    'is_deleted' =>0
                ];
                UserContact::insertGetId($dataarray);
            }
                
            DB::commit();
            return response()->json(['success' => 1, 'message' => "Contact details created successfully"]);
        }catch (\Exception $exception) {
            DB::rollback();
            $exception_message = $exception;
            return response()->json($exception_message);
        } catch (\Error $exception) {
            DB::rollback();
            $exception_message = $exception;
            return response()->json($exception_message);
        } catch (\Throwable $exception) {
            DB::rollback();
            $exception_message = $exception;
            return response()->json($exception_message);
        }
    }
    public function contactView(Request $request){
        $encrypted = $request->encrypted;
        $decryptedData = base64_decode($encrypted);
        $data = UserContact::where('contact_id',$decryptedData)->get()->toArray();
        return response()->json(["success"=>1 , "contactdata" => $data]);
    }
    public function contactDelete(Request $request){
        $encrypted = $request->encrypted;
        $decryptedData = base64_decode($encrypted);
        $dataarray = [
            'is_deleted'=>1,
            'updated_at'=>Carbon::now()

        ];
        $data = UserContact::where('contact_id',$decryptedData)->update($dataarray);
        return response()->json(["success"=>1 , "message" => "Contact Deleted successfully"]);
    }
}
