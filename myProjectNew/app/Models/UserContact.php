<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserContact extends Model {

    protected $table = 'tblusercontacts';
    public $timestamps = false;
    protected $primaryKey = 'contact_id';

}
